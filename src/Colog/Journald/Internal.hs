{-# LANGUAGE RecordWildCards #-}
module Colog.Journald.Internal
  ( LogAction (..)
  , Msg (..)
  , Message
  , logP
  , logS
  , renderToFields
  , journalAction
  ) where

import           Colog.Core.Action      (LogAction (..))
import           Colog.Core.Severity    (Severity (..))
import           Control.Monad.IO.Class
import           Data.Text              as Text
import           GHC.Stack              (CallStack, SrcLoc (..), callStack,
                                         getCallStack, withFrozenCallStack)
import           Prelude                hiding (log)
import qualified System.Posix.Syslog    as Syslog
import           Systemd.Journal        hiding (Priority (..))


-- * Types
------------------------------------------------------------------------------
data Msg priority
  = Msg
      { msgPriority :: !priority
      , msgStack    :: !CallStack
      , msgText     :: !Text
      }

type Message = Msg Syslog.Priority


-- * Functions
------------------------------------------------------------------------------
logP :: MonadIO m => Syslog.Priority -> Text -> m ()
logP msgPriority msgText =
  liftIO $ withFrozenCallStack (sendJournalFields $
                                renderToFields Msg{ msgStack = callStack, .. })

logS :: MonadIO m => Severity -> Text -> m ()
logS s = withFrozenCallStack (case s of
                                Debug   -> logP Syslog.Debug
                                Info    -> logP Syslog.Info
                                Warning -> logP Syslog.Warning
                                Error   -> logP Syslog.Error)

-- ** Internal functions
------------------------------------------------------------------------------
renderToFields :: Message -> JournalFields
renderToFields Msg{..} = msgpri <> message msgText <> fields
  where
    msgpri = priority . toEnum . fromEnum $ msgPriority
    fields = case getCallStack msgStack of
      []                    -> mempty
      (func', SrcLoc{..}):_ ->
        codeFunc (Text.pack func') <>
        codeLine srcLocStartLine <>
        codeFile srcLocFile

journalAction :: MonadIO m => LogAction m Message
journalAction  = LogAction $ liftIO . sendJournalFields . renderToFields
