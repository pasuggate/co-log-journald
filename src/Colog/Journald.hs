{-# LANGUAGE FlexibleContexts, PatternSynonyms, RecordWildCards #-}

module Colog.Journald
  (
    journalAction
  , log
  , logP
  , logS
  , LogAction (..)
  , Message
  , HasLog (..)
  , WithLog

    -- convenience patterns
  , pattern D
  , pattern I
  , pattern N
  , pattern W
  , pattern E
  , pattern C
  , pattern A

    -- convenience functions
  , logDebug
  , logInfo
  , logNotice
  , logWarn
  , logError
  , logCritical
  , logAlert
  )
where

import           Colog                   (HasLog (..), WithLog, logMsg)
import           Colog.Journald.Internal
import           Data.Text               as Text
import           GHC.Stack               (callStack, withFrozenCallStack)
import           Prelude                 hiding (log)
import qualified System.Posix.Syslog     as Syslog


-- * Convenience aliases and patterns
------------------------------------------------------------------------------
pattern D, I, W, E :: Syslog.Priority
pattern D <- Syslog.Debug    where D = Syslog.Debug
pattern I <- Syslog.Info     where I = Syslog.Info
pattern N <- Syslog.Notice   where N = Syslog.Notice
pattern W <- Syslog.Warning  where W = Syslog.Warning
pattern E <- Syslog.Error    where E = Syslog.Error
pattern C <- Syslog.Critical where C = Syslog.Critical
pattern A <- Syslog.Alert    where A = Syslog.Alert
{-# COMPLETE D, I, N, W, E, C, A #-}


-- * Functions
------------------------------------------------------------------------------
-- | Emit log entry of the given severity.
log :: WithLog env (Msg priority) m => priority -> Text -> m ()
log msgPriority msgText =
  withFrozenCallStack (logMsg Msg{ msgStack = callStack, .. })

-- ** Convenience functions
------------------------------------------------------------------------------
logDebug :: WithLog env Message m => Text -> m ()
logDebug  = withFrozenCallStack (log D)

logInfo :: WithLog env Message m => Text -> m ()
logInfo  = withFrozenCallStack (log I)

logNotice :: WithLog env Message m => Text -> m ()
logNotice  = withFrozenCallStack (log N)

logWarn :: WithLog env Message m => Text -> m ()
logWarn  = withFrozenCallStack (log W)

logError :: WithLog env Message m => Text -> m ()
logError  = withFrozenCallStack (log E)

logCritical :: WithLog env Message m => Text -> m ()
logCritical  = withFrozenCallStack (log C)

logAlert :: WithLog env Message m => Text -> m ()
logAlert  = withFrozenCallStack (log A)
