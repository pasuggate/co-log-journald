module MainSpec where

import           Test.Hspec

spec :: Spec
spec  = describe "Main" $ do
  it "'a'" $ do
    'a' `shouldBe` 'a'
