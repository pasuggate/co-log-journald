{-# LANGUAGE DeriveGeneric, DerivingStrategies, FlexibleContexts,
             FlexibleInstances, FunctionalDependencies,
             GeneralizedNewtypeDeriving, InstanceSigs, MultiParamTypeClasses,
             OverloadedStrings, PatternSynonyms, TemplateHaskell #-}

module Main where

import           Colog.Journald
import           Control.Lens           (makeClassy)
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           GHC.Generics           (Generic)
import           Prelude                hiding (log)


-- * Example types
------------------------------------------------------------------------------
data Env m
  = Env
      { _envServerPort :: !Int
      , _envLogAction  :: !(LogAction m Message)
      }
  deriving Generic

newtype App a
  = App
      { _unApp :: ReaderT (Env App) IO a
      }
  deriving Generic
  deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader (Env App))


-- * Lenses and instances
------------------------------------------------------------------------------
makeClassy ''Env
makeClassy ''App

------------------------------------------------------------------------------
instance HasLog (Env m) Message m where
  getLogAction :: Env m -> LogAction m Message
  getLogAction = _envLogAction
  {-# INLINE getLogAction #-}

  setLogAction :: LogAction m Message -> Env m -> Env m
  setLogAction newLogAction env = env { _envLogAction = newLogAction }
  {-# INLINE setLogAction #-}


-- * Functions
------------------------------------------------------------------------------
simpleEnv :: Env App
simpleEnv  = Env
  { _envServerPort = 8081
  , _envLogAction  = journalAction
  }

runApp :: Env App -> App a -> IO a
runApp env app = runReaderT (_unApp app) env

------------------------------------------------------------------------------
-- | Log a couple of messages.
example ::
     WithLog env Message m
  => m ()
example = do
  log D "First message..."
  log I "Second message..."
  log W "Third message..."
  log E "Fourth message..."


-- * Main program entry-point
------------------------------------------------------------------------------
main :: IO ()
main  = do
  putStrLn "Running example ..."
  logP D "Hi, ho"
  logP W "Oh noes!"
  logP E "Bloody hell, I need to change my shorts!!"
